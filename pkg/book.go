package pkg

type Book struct {
	Name string
	Author string
	Publisher string
}
