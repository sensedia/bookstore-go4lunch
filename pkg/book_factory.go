package pkg

func ProduceBooks() []Book {
	books := []Book{{
		Name:      "Knative in Action",
		Author:    "Jacques Chester",
		Publisher: "Manning",
	}, {
		Name:      "Event Streams in Action",
		Author:    "Alexander Dean, Valentin Crettaz",
		Publisher: "Manning",
	}, {
		Name:      "Cloud Native programming with Golang",
		Author:    "Martin Helmich, Mina Andrawos",
		Publisher: "Packt",
	},{
		Name:      "Principles of Data Science",
		Author:    "Sinan Ozdemir",
		Publisher: "Packt",
	},{
		Name:      "Beginning Kubernetes on the Google Cloud Platform",
		Author:    "Garbarino, Ernesto",
		Publisher: "Apress",
	},{
		Name:      "Mastering Microsoft Teams",
		Author:    "Hubbard, Melissa, Bailey, Matthew",
		Publisher: "Apress",
	}}
	return books
}
