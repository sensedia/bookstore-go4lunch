package routers

import (
	"bookstore/api/handlers"
	"bookstore/api/middleware"
	"github.com/gorilla/mux"
)

func InitRouter() *mux.Router {

	// Create a root router
	router := mux.NewRouter()

	// middleware declaration
	router.Use(middleware.JsonHeader)

	// TASK: Create a middleware to handle json serialization

	// Router for manning router
	manningRouter := router.Host("sensedia.book.manning").Subrouter()
	manningRouter.HandleFunc("/books",handlers.BooksManning).Methods("GET")

	// Router for packt router
	packtRouter := router.Host("sensedia.book.packt").Subrouter()
	packtRouter.HandleFunc("/books",handlers.BooksPackt).Methods("GET")

	// TASK: Create a subrouter that will handle only apress requests "sensedia.book.apress"

	return router

}