package handlers

import (
	"bookstore/pkg"
	"encoding/json"
	"net/http"
)

func BooksManning(w http.ResponseWriter, r *http.Request)  {
	allBooks := pkg.ProduceBooks()
	booksManning := filter(allBooks,"Manning")
	json,err :=json.Marshal(booksManning)
	if err != nil {
		return
	}
	w.WriteHeader(200)
	w.Write(json)
}

func BooksPackt(w http.ResponseWriter, r *http.Request)  {
	allBooks := pkg.ProduceBooks()	
	booksPackt := filter(allBooks,"Packt")
	json,err :=json.Marshal(booksPackt)
	if err != nil {
		return
	}
	w.WriteHeader(200)
	w.Write(json)
}

// TASK: Create a handler that will get only Apress Books

func filter(books []pkg.Book,publisher string) []pkg.Book {
	localCopy := make([]pkg.Book, len(books))
	copy(localCopy, books)
	filteredBooks := localCopy[:0]
	for _, book := range localCopy {
		if book.Publisher == publisher {
			filteredBooks = append(filteredBooks, book)
		}
	}
	return filteredBooks
}